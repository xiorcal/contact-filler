package me.xiorcal.poc.contactfiller.model

import android.net.Uri

data class ContactInfo(val id: Long, val displayName: String, val missingInfo: Set<Info>, val contactUri:Uri)
enum class Info {
    Photo, Phone, Email
}