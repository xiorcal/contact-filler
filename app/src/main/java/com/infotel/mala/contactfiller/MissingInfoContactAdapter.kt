package me.xiorcal.poc.contactfiller

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import me.xiorcal.poc.contactfiller.model.ContactInfo
import me.xiorcal.poc.contactfiller.model.Info


class MissingInfoContactAdapter(val context: Context, val contacts: List<ContactInfo>) : BaseAdapter() {
    override fun getView(position: Int, reuse: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = reuse ?: inflater.inflate(R.layout.one_contact_in_list, parent, false)

        val iconPhoto = view.findViewById<ImageView>(R.id.iconPhoto)
        val iconPhone = view.findViewById<ImageView>(R.id.iconPhone)
        val iconMail = view.findViewById<ImageView>(R.id.iconMail)
        val text = view.findViewById<TextView>(R.id.contactName)

        // from current item:
        val current = contacts[position]
        text.text = current.displayName
        iconPhoto.alpha = if (current.missingInfo.contains(Info.Photo)) 0.2f else 1f
        iconPhone.alpha = if (current.missingInfo.contains(Info.Phone)) 0.2f else 1f
        iconMail.alpha = if (current.missingInfo.contains(Info.Email)) 0.2f else 1f

        Log.d("contact", current.toString())
        return view
    }

    override fun getItem(position: Int): ContactInfo = contacts[position]


    override fun getItemId(position: Int): Long = contacts[position].id

    override fun getCount(): Int = contacts.size

}
