package me.xiorcal.poc.contactfiller

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.provider.ContactsContract
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.Toast
import me.xiorcal.poc.contactfiller.model.ContactInfo


private const val PERMISSIONS_REQUEST_READ_CONTACTS = 1
private const val TAG = "picker"
private var contacts: MutableList<ContactInfo>? = null


class Picker : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener, OnContactFetch {

    private lateinit var lstNames: ListView
    private lateinit var refreshLayout: SwipeRefreshLayout

    private var state: Parcelable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
        setSupportActionBar(findViewById(R.id.toolbar))

        lstNames = findViewById<ListView>(R.id.contact_list)
        refreshLayout = findViewById(R.id.refreshLayout)
        refreshLayout.setOnRefreshListener(this)
        lstNames.setOnItemClickListener { parent, view, position, id ->

            Log.i("clicked", "clicked: $position (pos), $id (id)")
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, id.toString())
            intent.data = uri
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()

        // fetch contact list and populate
        showContacts()
        if (state != null) {
            Log.d(TAG, "trying to restore listview state..")
            lstNames.onRestoreInstanceState(state)
        }
    }

    override fun onPause() {
        Log.d(TAG, "saving listview state @ onPause")
        state = lstNames.onSaveInstanceState()
        super.onPause()

    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_refresh -> {
            Log.i(TAG, "refresh from menu icon")
            showContacts(true)
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Show the contacts in the ListView.
     */
    private fun showContacts(forceRefresh: Boolean = false) {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSIONS_REQUEST_READ_CONTACTS)
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            getContacts(forceRefresh)

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts()
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onFetchComplete(contacts: List<ContactInfo>) {
        val adapter = MissingInfoContactAdapter(this, contacts)
        lstNames.adapter = adapter
        refreshLayout.isRefreshing = false
    }

    override fun onRefresh() {
        Log.i(TAG, "onRefresh called from SwipeRefreshLayout")
        showContacts(true)
    }

    /**
     * Read the name of all the contacts.
     *
     * @return a list of names.
     */
    private fun getContacts(forceRefresh: Boolean) {
        if (forceRefresh || contacts == null) {
            refreshLayout.isRefreshing = true

            val task = FetchContactAsync()
            task.onComplete = this
            task.execute(contentResolver)

        }
    }


}
