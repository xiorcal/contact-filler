package me.xiorcal.poc.contactfiller

import android.content.ContentResolver
import android.content.ContentUris
import android.os.AsyncTask
import android.provider.ContactsContract
import me.xiorcal.poc.contactfiller.model.ContactInfo
import me.xiorcal.poc.contactfiller.model.Info

class FetchContactAsync : AsyncTask<ContentResolver, Void, List<ContactInfo>>( ) {

    var onComplete: OnContactFetch? = null


    override fun doInBackground(vararg contentResolver: ContentResolver): List<ContactInfo> {
        val contacts = mutableListOf<ContactInfo>()
        // Get the ContentResolver
        val cr = contentResolver[0]
        // Get the Cursor of all the contacts
        val cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.Contacts.SORT_KEY_PRIMARY + " ASC")

        while (cursor != null && cursor.moveToNext()) {
            // Get the contacts name
            val id = cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID))
            val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
            val uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id)

            val missingInfo = mutableSetOf<Info>()

            val photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI))
            if (photoUri == null || photoUri == "") missingInfo.add(Info.Photo)

            val phone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
            if (phone == 0) missingInfo.add(Info.Phone)

            val mailCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=" + id, null, null)
            if (mailCursor == null || mailCursor.count == 0) {
                missingInfo.add(Info.Email)
            }
            mailCursor?.close()
            if (!missingInfo.isEmpty()) contacts!!.add(ContactInfo(id, name, missingInfo, uri))

        }
        // Close the cursor
        cursor?.close()
        return contacts
    }

    override fun onPostExecute(result: List<ContactInfo>?) {
        onComplete?.onFetchComplete(result!!)
    }
}
interface OnContactFetch{
    fun onFetchComplete(contacts: List<ContactInfo>)
}